<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NmspaceRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 */
abstract class Nmspace
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Nmspace", inversedBy="children")
     */
    public $parent;

    /**
     * @ORM\Column(type="string",unique=true)
     */
    public $name;

    /**
     * This field contains the full path of the namespace,
     * which is constructed by the tree structure of the namespace
     * 
     * @ORM\Column(type="text")
     */
    public $cached_fullpath;
 
    /**
     * @ORM\Column(type="text")
     */
    public $description;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    public $visibility;
    
    /**
     * @return int|NULL
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
