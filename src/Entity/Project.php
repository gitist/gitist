<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Nmspace")
     */
    public $nmspace;
    
    /**
     * @ORM\Column(type="string")
     */
    public $name;
    
    /**
     * @ORM\Column(type="text")
     */
    public $cached_fullpath;
    
    /**
     * @ORM\Column(type="text")
     */
    public $description;
    
    /**
     * @ORM\Column(type="integer")
     */
    public $visibility;
}

