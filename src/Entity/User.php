<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User extends Nmspace
{
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $password;

    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="group_members")
     */
    public $group;
}
