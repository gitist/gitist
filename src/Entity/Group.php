<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 * @ORM\Table(name="`group`")
 */
class Group extends Nmspace
{
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="group")
     */
    public $users;
}
