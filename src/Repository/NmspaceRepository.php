<?php

namespace App\Repository;

use App\Entity\Nmspace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Nmspace|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nmspace|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nmspace[]    findAll()
 * @method Nmspace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NmspaceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Nmspace::class);
    }

//    /**
//     * @return Nmspace[] Returns an array of Nmspace objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nmspace
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
